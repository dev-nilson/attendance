import mongoose from "mongoose";
import Question from "../models/questionsModel.js";

const getQuestions = async (req, res) => {
  const userId = req.user._id;
  const questions = await Question.find({ userId }).sort({ createdAt: -1 });
  res.status(200).json(questions);
};

const getQuestion = async (req, res) => {
  const userId = req.user._id;
  const { slug } = req.params;
  const questions = await Question.find({ userId, slug }).sort({
    createdAt: -1,
  });
  res.status(200).json(questions);
};

const createQuestion = async (req, res) => {
  const { question, answer, index, folderId, slug } = req.body;

  let emptyFields = [];

  if (!question) emptyFields.push("question");
  if (!answer) emptyFields.push("answer");

  if (emptyFields.length > 0) {
    return res
      .status(400)
      .json({ message: "Please fill in all the fields", emptyFields });
  }

  try {
    const userId = req.user._id;
    const questionToAdd = await Question.create({
      question,
      answer,
      index,
      userId,
      folderId,
      slug,
    });
    res.status(200).json(questionToAdd);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

const deleteQuestions = async (req, res) => {
  const userId = req.user._id;
  const { folderId } = req.params;

  if (!mongoose.Types.ObjectId.isValid(folderId)) {
    return res.status(404).json({ message: "Question not found" });
  }

  const question = await Question.deleteMany({ userId, folderId });

  if (!question) {
    return res.status(400).json({ message: "Question not found" });
  }

  res.status(200).json(question);
};

const updateQuestion = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({ message: "Question not found" });
  }

  const question = await Question.findOneAndUpdate(
    { _id: id },
    {
      ...req.body,
    },
    { new: true }
  );

  if (!question) {
    return res.status(400).json({ message: "Question not found" });
  }

  res.status(200).json(question);
};

export {
  getQuestions,
  getQuestion,
  createQuestion,
  deleteQuestions,
  updateQuestion,
};
