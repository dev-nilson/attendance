import jwt from "jsonwebtoken";
import User from "../models/usersModel.js";

const getUser = async (req, res) => {
  const _id = req.user._id;
  const user = await User.findOne({ _id });
  res.status(200).json(user);
};

const updateUser = async (req, res) => {
  const _id = req.user._id;

  const user = await User.findOneAndUpdate(
    { _id },
    {
      ...req.body,
    },
    { new: true }
  );

  if (!user) {
    return res.status(400).json({ message: "User not found" });
  }

  res.status(200).json(user);
};

const creatToken = (id) => {
  return jwt.sign({ id }, process.env.SECRET_KEY, { expiresIn: "3d" });
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.login(email, password);
    const token = creatToken(user._id);
    res.status(200).json({ email, token });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const signupUser = async (req, res) => {
  const { email, password, confirmation, name } = req.body;

  try {
    const user = await User.signup(email, password, confirmation, name);
    const token = creatToken(user._id);
    res.status(200).json({ email, token });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

export { getUser, updateUser, loginUser, signupUser };
