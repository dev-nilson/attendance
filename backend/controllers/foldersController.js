import mongoose from "mongoose";
import Folder from "../models/foldersModel.js";

const getFolders = async (req, res) => {
  const userId = req.user._id;
  const folders = await Folder.find({ userId }).sort({ updatedAt: -1 });
  res.status(200).json(folders);
};

const updateFolder = async (req, res) => {
  const { id } = req.params;

  try {
    const updatedFolder = await Folder.findOneAndUpdate(
      { _id: id },
      { $currentDate: { updatedAt: true } },
      { new: true }
    );

    res.status(200).json(updatedFolder);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const createFolder = async (req, res) => {
  const { title } = req.body;

  let emptyFields = [];
  if (!title) emptyFields.push("title");

  if (emptyFields.length > 0) {
    return res
      .status(400)
      .json({ message: "Please fill in all the fields", emptyFields });
  }

  try {
    const userId = req.user._id;
    // TO DO: maybe get rid of slug?
    const slug = title;
    const folderToAdd = await Folder.create({ title, slug, userId });
    return res.status(200).json(folderToAdd);
  } catch (err) {
    return res.status(400).json({ error: err.message });
  }
};

const deleteFolder = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({ message: "Folder not found" });
  }

  const folder = await Folder.findByIdAndDelete({ _id: id });

  if (!folder) {
    return res.status(400).json({ message: "Folder not found" });
  }

  res.status(200).json(folder);
};

export { getFolders, updateFolder, createFolder, deleteFolder };
