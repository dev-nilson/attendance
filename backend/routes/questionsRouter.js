import express from "express";
import { auth } from "../middleware/auth.js";
import {
  createQuestion,
  deleteQuestions,
  getQuestion,
  getQuestions,
  updateQuestion,
} from "../controllers/questionsController.js";

const router = express.Router();

router.use(auth);

router.get("/", getQuestions);
router.get("/:slug", getQuestion);
router.post("/", createQuestion);
router.delete("/:folderId", deleteQuestions);
router.put("/:id", updateQuestion);

export default router;
