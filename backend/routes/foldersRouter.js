import express from "express";
import { auth } from "../middleware/auth.js";
import {
  getFolders,
  createFolder,
  deleteFolder,
  updateFolder,
} from "../controllers/foldersController.js";

const router = express.Router();

router.use(auth);

router.get("/", getFolders);
router.post("/", createFolder);
router.put("/:id", updateFolder);
router.delete("/:id", deleteFolder);

export default router;
