import express from "express";
import { auth } from "../middleware/auth.js";
import {
  getUser,
  updateUser,
  loginUser,
  signupUser,
} from "../controllers/usersController.js";

const router = express.Router();

router.post("/login", loginUser);
router.post("/signup", signupUser);

router.use(auth);
router.get("/", getUser);
router.put("/", updateUser);

export default router;
