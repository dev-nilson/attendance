import mongoose from "mongoose";
import bcrypt from "bcrypt";
import validator from "validator";

const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    maxlength: 50,
  },
});

userSchema.statics.signup = async function (
  email,
  password,
  confirmation,
  name
) {
  if (!email || !password) throw Error("All fields are required");
  if (!validator.isEmail(email)) throw Error("Email is invalid");
  if (!validator.isStrongPassword(password)) throw Error("Password is weak");
  if (password !== confirmation) throw Error("Passwords do not match");
  if (name.length > 50) throw Error("Name exceeds 50 character limit");

  const exists = await this.findOne({ email });
  if (exists) throw Error("Email already in use");

  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);

  const user = await this.create({ email, password: hash, name });
  return user;
};

userSchema.statics.login = async function (email, password) {
  if (!email || !password) throw Error("All fields are required");

  const user = await this.findOne({ email });
  if (!user) throw Error("Incorrect email or password");

  const match = await bcrypt.compare(password, user?.password);
  if (!match) throw Error("Incorrect email or password");

  return user;
};

export default mongoose.model("User", userSchema);
