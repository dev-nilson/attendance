import mongoose from "mongoose";

const Schema = mongoose.Schema;

const questionSchema = new Schema(
  {
    question: {
      type: String,
      required: true,
    },
    answer: {
      type: String,
      required: true,
    },
    progress: {
      type: Number,
      default: 0,
    },
    index: {
      type: Number,
    },
    userId: {
      type: String,
      required: true,
    },
    folderId: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

export default mongoose.model("Question", questionSchema);
