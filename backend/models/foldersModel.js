import mongoose from "mongoose";

const Schema = mongoose.Schema;

const folderSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      validate: {
        validator: noSpacesValidator,
        message: (props) => `${props.value} cannot contain spaces`,
      },
    },
    slug: {
      type: String,
      required: true,
      unique: { sparse: true },
    },
    userId: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

folderSchema.index({ userId: 1, slug: 1 }, { unique: true });

function noSpacesValidator(value) {
  return !/\s/.test(value);
}

export default mongoose.model("Folder", folderSchema);
