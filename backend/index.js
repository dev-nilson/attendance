import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import cors from "cors";
import foldersRoutes from "./routes/foldersRouter.js";
import questionsRoutes from "./routes/questionsRouter.js";
import usersRoutes from "./routes/usersRouter.js";

dotenv.config();
const app = express();

app.use(cors());
app.use(express.json());

app.use("/api/questions", questionsRoutes);
app.use("/api/users", usersRoutes);
app.use("/api/folders", foldersRoutes)

mongoose.set("strictQuery", false);
mongoose.connect(process.env.MONGO_URI)
   .then(() => {
      app.listen(process.env.PORT_NUMBER, () => {
         console.log(`Listening on port ${process.env.PORT_NUMBER}...`)
      });
   })
   .catch((err) => { console.log(err) })
