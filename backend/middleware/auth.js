import jwt from "jsonwebtoken";
import User from "../models/usersModel.js";

export const auth = async (req, res, next) => {
   const { authorization } = req.headers;

   if (!authorization) {
      return res.status(401).json({ message: "Authorization required" })
   }

   const token = authorization.split(" ")[1];

   try {
      const { id } = jwt.verify(token, process.env.SECRET_KEY);
      req.user = await User.findOne({ _id: id }).select("id");
      next();
   } catch (error) {
      res.status(401).json({ message: "Request is not authorized" });
   }
}