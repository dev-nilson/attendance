# Quizhub
Quizhub is an innovative web application that combines the best features of GitHub, Quizlet, and Tinder to create a unique and engaging platform for learning new technologies, languages, and subjects.

#### 🧩 Features
---
* Interactive Study Sets
* Tinder-Inspired Learning
* Progress Track Chart

#### ⭐ Status
---
* Development

#### 📘 Tech Stack
---
* Mongo
* Express
* React
* Node
* TypeScript

#### 📌 Demo
---
* **Website:** https://quizhub-frontend.onrender.com
* **Email:** `demo@mail.com`
* **Password:** `P@ssw0rd`


