import { useState } from "react";
import Disclaimer from "../Disclaimer/Disclaimer";
import Button from "../Button/Button";
import styles from "./QuestionFormFooter.module.css";

type QuestionFormFooterProps = {
  title: string;
  showDanger: boolean;
  editMode: boolean;
  isAtMaximum: boolean;
  isAtMinimum: boolean;
};

function QuestionFormFooter({
  title,
  showDanger,
  editMode,
  isAtMaximum,
  isAtMinimum,
}: QuestionFormFooterProps) {
  const [isLoading, setIsLoading] = useState(false);

  const handleClick = () => {
    if (!isAtMaximum) {
      setIsLoading(true);
    }
  };

  return (
    <footer>
      <Disclaimer
        text={
          isAtMaximum
            ? "You have exceeded the maximum number of 50 cards in this folder"
            : editMode
            ? "Editing a flashcard will reset its progress. Please be aware of this when making changes."
            : "You are creating a private folder in your personal account."
        }
        danger={isAtMaximum}
      />
      <div className={styles.button}>
        {editMode ? (
          <Button type="submit" onClick={handleClick}>
            {isLoading ? "Saving..." : "Save Changes"}
          </Button>
        ) : (
          <Button
            type="submit"
            disabled={
              title.length === 0 || showDanger || isAtMaximum || isAtMinimum
            }
            onClick={handleClick}
          >
            {isLoading ? "Creating..." : "Create Folder"}
          </Button>
        )}
      </div>
    </footer>
  );
}

export default QuestionFormFooter;
