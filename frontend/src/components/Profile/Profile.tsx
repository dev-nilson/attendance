import { useState } from "react";
import Button from "../Button/Button";
import ProfileEdit from "../ProfileEdit/ProfileEdit";
import styles from "./Profile.module.css";

type ProfileProps = {
  name: string;
  email: string;
  foldersCount: number;
  country: string;
  time: any;
  fetchUser: any;
};

function Profile({
  name,
  email,
  foldersCount,
  country,
  time,
  fetchUser,
}: ProfileProps) {
  const [editMode, setEditMode] = useState(false);

  const toggleEditMode = () => {
    setEditMode((prevEditMode) => !prevEditMode);
  };

  return (
    <section className={styles.profile}>
      <div className={styles.profileContainer}>
        <img
          className={styles.avatar}
          src={`https://api.dicebear.com/6.x/identicon/svg?seed=${email}`}
          alt="avatar"
        />
        <div className={styles.info}>
          {editMode ? (
            <ProfileEdit
              name={name}
              fetchUser={fetchUser}
              toggleEditMode={toggleEditMode}
            />
          ) : (
            <div className={styles.bio}>
              <h2 className={styles.name} title={name}>{name}</h2>
              <p className={styles.username} title={email}>{email}</p>
            </div>
          )}
          {!editMode && (
            <div className={styles.button}>
              <Button variant="secondary" onClick={toggleEditMode}>
                Edit profile
              </Button>
            </div>
          )}
        </div>
      </div>
      <div className={styles.stats}>
        <div className={styles.stat}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="18"
            height="18"
            viewBox="0 0 24 24"
            fill="#7d8590"
          >
            <path d="M20 5h-8.586L9.707 3.293A.997.997 0 0 0 9 3H4c-1.103 0-2 .897-2 2v14c0 1.103.897 2 2 2h16c1.103 0 2-.897 2-2V7c0-1.103-.897-2-2-2zM4 19V7h16l.002 12H4z"></path>
          </svg>
          <p>{foldersCount} Folders</p>
        </div>
        <div className={styles.stat}>
          <svg viewBox="0 0 16 16" fill="#7d8590" height="18" width="18">
            <path
              fillRule="evenodd"
              d="M11.536 3.464a5 5 0 010 7.072L8 14.07l-3.536-3.535a5 5 0 117.072-7.072v.001zm1.06 8.132a6.5 6.5 0 10-9.192 0l3.535 3.536a1.5 1.5 0 002.122 0l3.535-3.536zM8 9a2 2 0 100-4 2 2 0 000 4z"
            />
          </svg>
          <p>{country}</p>
        </div>
        <div className={styles.stat}>
          <svg width="18" height="18" viewBox="0 0 24 24" fill="#7d8590">
            <path d="M12 20c4.4 0 8-3.6 8-8s-3.6-8-8-8-8 3.6-8 8 3.6 8 8 8m0-18c5.5 0 10 4.5 10 10s-4.5 10-10 10S2 17.5 2 12 6.5 2 12 2m5 11.9l-.7 1.3-5.3-2.9V7h1.5v4.4l4.5 2.5z" />
          </svg>
          <p>{time}</p>
        </div>
      </div>
    </section>
  );
}

export default Profile;
