import { Link } from "react-router-dom";
import styles from "./Chart.module.css";

type ChartProps = {
  data: Question[];
};

const Chart = ({ data }: ChartProps) => {
  return (
    <div className={styles.chart}>
      {Array.from({ length: 100 }, (_, index) => {
        if (data[index]) {
          return (
            <Link
              to={`/study/${data[index].slug}?question=${index + 1}`}
              key={index}
            >
              <div title={`Question ${index + 1}`}>
                <div
                  className={
                    data.length === 0 ? styles.squareLoading : styles.square
                  }
                  style={{
                    background: `rgba(57, 211, 83, ${
                      data[index].progress / 100
                    })`,
                  }}
                />
              </div>
            </Link>
          );
        } else {
          return (
            <div
              key={index}
              title={`Question ${index + 1}`}
              className={
                data.length === 0 ? styles.squareLoading : styles.square
              }
            />
          );
        }
      })}
    </div>
  );
};

export default Chart;
