import styles from "./ContributionCardSkeleton.module.css";

type ContributionCardSkeletonProps = {
  label: string;
};

function ContributionCardSkeleton({ label }: ContributionCardSkeletonProps) {
  const questions: any[] = [1, 2, 3];

  return (
    <div className={styles.contributionCard}>
      <h3 className={styles.label}>{label}</h3>
      <div className={styles.questions}>
        {questions.map((question) => (
          <div className={styles.question} key={question}>
            <div className={styles.text}>
              <div className={styles.link}></div>
              <div className={styles.guess}></div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default ContributionCardSkeleton;
