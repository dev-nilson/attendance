import styles from "./Tooltip.module.css";

type TooltipProps = {
  children: any;
  variant?: "success" | "danger" | "warning";
};

function Tooltip({ children, variant }: TooltipProps) {
  const tooltipClasses = `${styles.tooltip} ${
    variant === "danger"
      ? styles.dangerTooltip
      : variant === "warning"
      ? styles.warningTooltip
      : variant === "success"
      ? styles.successTooltip
      : ""
  }`;

  return <div className={tooltipClasses}>{children}</div>;
}

export default Tooltip;
