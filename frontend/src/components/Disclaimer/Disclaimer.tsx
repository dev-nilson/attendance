import styles from "./Disclaimer.module.css";

type DisclaimerProps = {
  text: string;
  danger?: boolean;
};

function Disclaimer({ text, danger }: DisclaimerProps) {
  return (
    <div className={styles.disclaimer}>
      {danger ? (
        <svg
          fill="none"
          stroke="#f85149"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          viewBox="0 0 24 24"
          height="16px"
          width="16px"
        >
          <path d="M10.29 3.86L1.82 18a2 2 0 001.71 3h16.94a2 2 0 001.71-3L13.71 3.86a2 2 0 00-3.42 0zM12 9v4M12 17h.01" />
        </svg>
      ) : (
        <svg
          fill="none"
          stroke="#7d8590"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          viewBox="0 0 24 24"
          height="16px"
          width="16px"
        >
          <path d="M22 12 A10 10 0 0 1 12 22 A10 10 0 0 1 2 12 A10 10 0 0 1 22 12 z" />
          <path d="M12 8v4M12 16h.01" />
        </svg>
      )}

      <p className={`${danger ? styles.dangerText : styles.text}`}>{text}</p>
    </div>
  );
}

export default Disclaimer;
