import Label from "../Label/Label";
import Input from "../Input/Input";
import styles from "./Question.module.css";

type QuestionProps = {
  index: number;
  question: string;
  answer: string;
  handleInputChange: (e: any, index: number) => void;
};

function Question({
  index,
  question,
  answer,
  handleInputChange,
}: QuestionProps) {
  return (
    <div className={styles.question}>
      <div className={styles.input}>
        <Label htmlFor={`question-${index}`}>Question {index + 1}</Label>
        <Input
          id={`question-${index}`}
          name="question"
          type="text"
          value={question}
          onChange={(e) => handleInputChange(e, index)}
        />
      </div>
      <div className={styles.input}>
        <Label htmlFor={`answer-${index}`}>Answer {index + 1}</Label>
        <Input
          id={`answer-${index}`}
          name="answer"
          type="text"
          value={answer}
          onChange={(e) => handleInputChange(e, index)}
        />
      </div>
    </div>
  );
}

export default Question;
