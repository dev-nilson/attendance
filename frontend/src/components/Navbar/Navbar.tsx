import { Link } from "react-router-dom";
import { useAuthContext } from "../../hooks/useAuthContext";
import NavbarAvatar from "../NavbarAvatar/NavbarAvatar";
import Dropdown from "../Dropdown/Dropdown";
import styles from "./Navbar.module.css";

type NavbarProps = {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
};

function Navbar({ isOpen, setIsOpen }: NavbarProps) {
  const { user } = useAuthContext();

  return (
    <nav className={styles.navbar} onClick={() => setIsOpen(false)}>
      <div className={styles.navbarContainer}>
        <Link to="/">
          <h1 className={styles.logo}>Quizhub</h1>
        </Link>
        <div
          className={styles.controls}
          onClick={(e) => {
            e.stopPropagation();
            setIsOpen(!isOpen);
          }}
        >
          <div>
            <NavbarAvatar seed={user?.email} />
          </div>
        </div>
        {isOpen && <Dropdown />}
      </div>
    </nav>
  );
}

export default Navbar;
