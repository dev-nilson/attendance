import styles from "./Modal.module.css";

type ModalProps = {
  setIsModalOpen: (arg: boolean) => void;
  children: React.ReactNode;
};

function Modal({ setIsModalOpen, children }: ModalProps) {
  const handleClick = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={styles.backdrop} onClick={handleClick}>
      <div className={styles.modal} onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
}

export default Modal;
