import { useState } from "react";
import slugify from "slugify";
import { doesFieldExist, hasSpaces } from "../../utils/helpers";
import Input from "../Input/Input";
import Label from "../Label/Label";
import Tooltip from "../Tooltip/Tooltip";
import styles from "./QuestionFormHeader.module.css";

type QuestionFormHeaderProps = {
  folders: any;
  title: string;
  setTitle: any;
  showDanger: boolean;
  setShowDanger: any;
  editMode: boolean;
};

function QuestionFormHeader({
  folders,
  title,
  setTitle,
  showDanger,
  setShowDanger,
  editMode,
}: QuestionFormHeaderProps) {
  const [showWarning, setShowWarning] = useState(false);
  const [showSucces, setShowSucces] = useState(false);

  const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const maxLength = 50;
    const inputValue = e.target.value;

    if (inputValue.length > maxLength) return;

    if (hasSpaces(inputValue)) {
      setShowWarning(true);
      setShowSucces(false);
    } else {
      setShowSucces(true);
      setShowWarning(false);
    }

    if (
      doesFieldExist(folders, "title", slugify(inputValue, { trim: false }))
    ) {
      setShowDanger(true);
      setShowWarning(false);
      setShowSucces(false);
    } else setShowDanger(false);

    setTitle(inputValue);
  };

  return (
    <header className={styles.solo}>
      <div style={{ position: "relative" }}>
        <div className={styles.header}>
          <div className={styles.input}>
            <Label htmlFor="title" required={!editMode}>
              Folder name
            </Label>
            <Input
              id="title"
              name="question"
              value={title}
              onChange={handleTitleChange}
              variant="secondary"
              readOnly={editMode}
            />
          </div>
        </div>

        {showWarning && (
          <Tooltip variant="warning">
            Your new folder will be created as{" "}
            <b>{slugify(title, { trim: false, remove: /[^a-zA-Z0-9-\s]/g })}</b>
          </Tooltip>
        )}
        {showDanger && (
          <Tooltip variant="danger">
            The folder <b>{slugify(title, { trim: false })}</b> already exists
            on this account
          </Tooltip>
        )}
        {showSucces && title.length > 0 && (
          <Tooltip variant="success">
            <b>{slugify(title, { trim: false, remove: /[^a-zA-Z0-9-\s]/g })}</b>{" "}
            is available
          </Tooltip>
        )}
      </div>
    </header>
  );
}

export default QuestionFormHeader;
