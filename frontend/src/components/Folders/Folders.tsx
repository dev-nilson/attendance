import FolderCard from "../FolderCard/FolderCard";

type FoldersProps = {
  data: Folder[];
};

export default function Folders({ data }: FoldersProps) {
  return (
    <div>
      {data.map((folder: Folder) => (
        <FolderCard
          key={folder._id}
          id={folder._id}
          slug={folder.slug}
          updatedAt={folder.updatedAt}
        />
      ))}
    </div>
  );
}
