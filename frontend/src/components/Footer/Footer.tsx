import { Link } from "react-router-dom";
import styles from "./Footer.module.css";

function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <Link to="/">
          <h3 className={styles.logo}>Quizhub</h3>
        </Link>
        <p className={styles.text}>Made with by 💙 Denilson Lemus</p>
      </div>
    </footer>
  );
}

export default Footer;
