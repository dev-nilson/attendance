import styles from "./Alert.module.css";

type AlertProps = {
  text: string;
  variant?: "success" | "warning" | "danger";
};

function Alert({ text, variant }: AlertProps) {
  const alertClasses = `${styles.alert} ${
    variant === "danger"
      ? styles.dangerAlert
      : variant === "warning"
      ? styles.warningAlert
      : variant === "success"
      ? styles.successAlert
      : ""
  }`;

  return (
    <div className={alertClasses}>
      <p>{text}</p>
    </div>
  );
}

export default Alert;
