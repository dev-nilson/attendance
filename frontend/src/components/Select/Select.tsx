import styles from "./Select.module.css";

type SelectProps = {
  value: string;
  setValue: (arg: string) => void;
};

function Select({ value, setValue }: SelectProps) {
  return (
    <select
      className={styles.select}
      defaultValue={"DEFAULT"}
      onChange={(e) => setValue(e.target.value)}
    >
      <option value={value} disabled hidden>
        Sort
      </option>
      <option value="updatedAt">Last</option>
      <option value="title">Name</option>
    </select>
  );
}

export default Select;
