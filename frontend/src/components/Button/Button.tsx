import { ButtonHTMLAttributes } from "react";
import styles from "./Button.module.css";

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  variant?: "primary" | "secondary" | "danger" | "info";
};

function Button({ variant, children, ...props }: ButtonProps) {
  const buttonClasses = `${styles.button} ${
    variant === "secondary"
      ? styles.secondaryButton
      : variant === "danger"
      ? styles.dangerButton
      : variant === "info"
      ? styles.infoButton
      : null
  }`;

  return (
    <button className={buttonClasses} {...props}>
      {children}
    </button>
  );
}

export default Button;
