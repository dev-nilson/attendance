import { useState } from "react";
import TinderCard from "react-tinder-card";
import styles from "./Card.module.css";

type CardProps = {
  question: Question;
  swiped: (direction: string, question: Question, index: number) => void;
  index: number;
  childRefs: any;
};

function Card({ question, swiped, index, childRefs }: CardProps) {
  const [showQuestion, setShowQuestion] = useState(true);
  const cardClasses = `${styles.card} ${!showQuestion && styles.flip}`;

  const handleFlip = () => {
    setShowQuestion((prevState) => !prevState);
  };

  return (
    <TinderCard
      className={styles.tinderCard}
      key={question._id}
      ref={childRefs[index]}
      preventSwipe={["up", "down"]}
      onSwipe={(dir) => swiped(dir, question, index)}
    >
      <div className={cardClasses} onClick={handleFlip}>
        <div className={styles.front}>
          <p className={styles.cardText}>{question.question}</p>
        </div>
        <div className={styles.back}>
          <p className={styles.cardText}>{question.answer}</p>
        </div>
      </div>
    </TinderCard>
  );
}

export default Card;
