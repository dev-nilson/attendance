import LoadingSpinner from "../LoadingSpinner/LoadingSpinner";
import styles from "./ProfileSkeleton.module.css";

function ProfileSkeleton() {
  return (
    <section className={styles.profile}>
      <div className={styles.avatar}>
        <LoadingSpinner />
      </div>
      <div className={styles.bio}>
        <div className={styles.name} />
        <div className={styles.username} />
      </div>
      <div className={styles.stats}>
        <div className={styles.stat} />
        <div className={styles.stat} />
        <div className={styles.stat} />
      </div>
    </section>
  );
}

export default ProfileSkeleton;
