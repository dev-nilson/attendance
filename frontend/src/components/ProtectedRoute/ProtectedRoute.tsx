import { Navigate } from "react-router-dom";
import { useAuthContext } from "../../hooks/useAuthContext";

type ProtectedRouteProps = {
  children: JSX.Element;
};

function ProtectedRoute({ children }: ProtectedRouteProps) {
  const { user, isLoading } = useAuthContext();

  if (!isLoading && !user) {
    return <Navigate to="/login" replace />;
  }

  return children;
}

export default ProtectedRoute;
