import styles from "./FolderCardSkeleton.module.css";

function FolderCardSkeleton() {
  return (
    <div className={styles.folderCardSkeleton}>
      {[...Array(5)].map((_, index) => (
        <div className={styles.folder} key={index}>
          <div className={styles.title}></div>
          <div className={styles.subtitle}></div>
        </div>
      ))}
    </div>
  );
}

export default FolderCardSkeleton;
