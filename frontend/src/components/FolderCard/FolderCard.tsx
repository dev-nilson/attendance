import { useState } from "react";
import { Link } from "react-router-dom";
import ReactTimeAgo from "react-time-ago";
import { useAuthContext } from "../../hooks/useAuthContext";
import { useFoldersContext } from "../../hooks/useFoldersContext";
import Button from "../Button/Button";
import Input from "../Input/Input";
import Label from "../Label/Label";
import Modal from "../Modal/Modal";
import styles from "./FolderCard.module.css";

type FolderCardProps = {
  slug: string;
  id: string;
  updatedAt: Date;
};

function FolderCard({ slug, id, updatedAt }: FolderCardProps) {
  const { user } = useAuthContext();
  const { dispatch } = useFoldersContext();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [confirmation, setConfirmation] = useState("");

  const handleDelete = () => {
    if (confirmation === slug) deleteQuestions();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setConfirmation(e.target.value);
  };

  const deleteQuestions = async () => {
    await fetch(
      `${import.meta.env.VITE_BASE_URL}/api/questions/${id}`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${user?.token}`,
        },
      }
    );

    const response = await fetch(
      `${import.meta.env.VITE_BASE_URL}/api/folders/${id}`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    const data = await response.json();
    dispatch({ type: "DELETE_FOLDER", payload: data });
  };

  return (
    <div className={styles.folderCard}>
      <div className={styles.info}>
        <Link to={`/study/${slug}`} className={styles.title}>
          {slug}
        </Link>
        <div className={styles.time}>
          Updated <ReactTimeAgo date={new Date(updatedAt)} locale="en-US" />
        </div>
      </div>
      <div className={styles.controls}>
        <Link to={`/edit/${slug}`}>
          <Button variant="secondary">Edit</Button>
        </Link>
        <Button variant="danger" onClick={() => setIsModalOpen(true)}>
          Delete
        </Button>
      </div>
      {isModalOpen && (
        <Modal setIsModalOpen={setIsModalOpen}>
          <div className={styles.modal}>
            <div className={styles.modalHead}>
              <p>Are you absolutely sure?</p>
            </div>
            <div className={styles.modalBody}>
              <p>
                This action cannot be undone. This will permanently delete the{" "}
                <b>{slug}</b> folder, questions, answers, and progress.
              </p>
              <Label htmlFor="confirmation">
                Please type <b>{slug}</b> to confirm
              </Label>
              <Input
                id="confirmation"
                value={confirmation}
                onChange={handleChange}
              />
              <Button
                variant="danger"
                disabled={confirmation !== slug}
                onClick={handleDelete}
              >
                Delete this folder
              </Button>
            </div>
          </div>
        </Modal>
      )}
    </div>
  );
}

export default FolderCard;
