import styles from "./NavbarAvatar.module.css";

type NavbarAvatarProps = {
  seed: string;
};

function NavbarAvatar({ seed }: NavbarAvatarProps) {
  return (
    <div className={styles.navbarAvatar}>
      <div className={styles.menu}>
        <img
          className={styles.avatar}
          src={`https://api.dicebear.com/6.x/identicon/svg?seed=${seed}`}
          alt="avatar"
        />
        <svg viewBox="0 0 24 24" fill="#fff" height="9px" width="9px">
          <path d="M11.178 19.569a.998.998 0 001.644 0l9-13A.999.999 0 0021 5H3a1.002 1.002 0 00-.822 1.569l9 13z" />
        </svg>
      </div>
    </div>
  );
}

export default NavbarAvatar;
