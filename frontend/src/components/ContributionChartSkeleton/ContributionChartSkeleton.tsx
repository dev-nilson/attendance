import { Link } from "react-router-dom";
import Chart from "../Chart/Chart";
import styles from "./ContributionChartSkeleton.module.css";

export default function ContributionChartSkeleton() {
  return (
    <div className={styles.contributionChartSkeleton}>
      <p className={styles.label}>
        Loading...
      </p>
      <Chart data={[]} />
      <div className={styles.info}>
        <Link className={styles.link} to={"#"}>
          Learn how we count progress
        </Link>
        <div className={styles.example}>
          <span>Less</span>
          {Array.from({ length: 5 }, (_, index) => (
            <div key={index} className={styles.square}>
              <div
                className={styles.square}
                style={{
                  background: `rgba(57, 211, 83, ${index * 0.2})`,
                }}
              ></div>
            </div>
          ))}
          <span>More</span>
        </div>
      </div>
    </div>
  );
}
