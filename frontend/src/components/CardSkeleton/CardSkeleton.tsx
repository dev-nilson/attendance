import styles from "./CardSkeleton.module.css";

function CardSkeleton() {
  return (
    <div className={styles.cardSkeleton}>
      <div className={styles.card}>
        <div></div>
      </div>
    </div>
  );
}

export default CardSkeleton;
