import { LabelHTMLAttributes } from "react";
import styles from "./Label.module.css";

type LabelProps = {
  required?: boolean;
} & LabelHTMLAttributes<HTMLLabelElement>;

function Label({ required, children, ...props }: LabelProps) {
  return (
    <label className={styles.label} {...props}>
      {children} {required && <span className={styles.star}>*</span>}
    </label>
  );
}

export default Label;
