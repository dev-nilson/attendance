import { Link } from "react-router-dom";
import { useLogout } from "../../hooks/useLogout";
import styles from "./Dropdown.module.css";

function Dropdown() {
  const { logout } = useLogout();

  const handleLogout = () => {
    logout();
  };

  return (
    <div className={styles.dropdown}>
      <div className={styles.body}>
        <Link to="settings" className={styles.link}>
          <div className={styles.option}>Settings</div>
        </Link>
      </div>
      <div className={styles.footer}>
        <div className={styles.option} onClick={handleLogout}>
          Sign out
        </div>
      </div>
    </div>
  );
}

export default Dropdown;
