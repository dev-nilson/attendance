import { useState } from "react";
import { Outlet } from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";

function Layout() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="layout">
      <Navbar isOpen={isOpen} setIsOpen={setIsOpen} />
      <main className="content" onClick={() => setIsOpen(false)}>
        <Outlet />
      </main>
      <Footer />
    </div>
  );
}

export default Layout;
