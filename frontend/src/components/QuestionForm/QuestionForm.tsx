import { useState } from "react";
import { useNavigate } from "react-router-dom";
import slugify from "slugify";
import { v4 as uuidv4 } from "uuid";
import { useAuthContext } from "../../hooks/useAuthContext";
import { useFoldersContext } from "../../hooks/useFoldersContext";
import { createFolder, updateFolder } from "../../utils/folders";
import { createQuestion, deleteQuestions } from "../../utils/questions";
import Button from "../Button/Button";
import Question from "../Question/Question";
import QuestionFormHeader from "../QuestionFormHeader/QuestionFormHeader";
import QuestionFormFooter from "../QuestionFormFooter/QuestionFormFooter";
import styles from "./QuestionForm.module.css";

type QuestionFormProps = {
  slug?: string;
  questions?: Question[];
  editMode: boolean;
};

const emptyField = {
  _id: uuidv4(),
  question: "",
  answer: "",
  progress: 0,
  index: 0,
  slug: "",
  folderId: "",
};

function QuestionForm({ editMode, slug, questions }: QuestionFormProps) {
  const minCards = 3;
  const maxCards = 5;
  const { user } = useAuthContext();
  const { folders } = useFoldersContext();
  const navigate = useNavigate();
  const [isAtMaximum, setIsAtMaximum] = useState(false);
  const [isAtMinimum, setIsAtMinimum] = useState(true);
  const [showDanger, setShowDanger] = useState(false);
  const [title, setTitle] = slug ? useState(slug) : useState("");
  const [inputs, setInputs] = questions
    ? useState<Question[]>(questions)
    : useState<Question[]>([emptyField]);
  const folderId = folders.find((obj: Folder) => obj.slug === slug)?._id;

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const { name, value } = e.target;
    const newInputs = [...inputs];
    newInputs[index] = {
      ...newInputs[index],
      [name]: value,
    };

    setInputs(newInputs);
  };

  const handleClick = () => {
    if (inputs.length === maxCards) {
      setIsAtMaximum(true);
      return;
    }

    if (inputs.length >= minCards - 1) {
      setIsAtMinimum(false);
    }

    const lastInput = inputs[inputs.length - 1];
    // prevent new card if previous card hasn't been used
    if (lastInput.question.trim() && lastInput.answer.trim()) {
      setInputs([...inputs, { ...emptyField, _id: uuidv4() }]);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (
      !title ||
      showDanger ||
      inputs.length > maxCards ||
      inputs.length < minCards
    ) {
      return;
    }

    if (editMode) {
      await updateFolder(folderId, user.token);
      await deleteQuestions(folderId, user.token);
      await saveQuestions(folderId, slug!);
    } else {
      /* 
      await deleteFolder(folderId, user.token);
      await deleteQuestions(folderId, user.token); 
      */
      const { _id, slug } = await saveFolder();
      saveQuestions(_id, slug);
    }

    navigate("/");
  };

  const saveFolder = async () => {
    const folderToSave = {
      title: slugify(title, { trim: false, remove: /[^a-zA-Z0-9-\s]/g }),
    };

    const folder = await createFolder(folderToSave as Folder, user.token);
    return await folder.json();
  };

  const saveQuestions = async (folderId: string, slug: string) => {
    for (const [index, input] of inputs.entries()) {
      const questionToSave = {
        question: input.question.trim(),
        answer: input.answer.trim(),
        folderId,
        slug,
        index: index + 1,
      };

      await createQuestion(questionToSave as Question, user.token);
    }
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit}>
      <QuestionFormHeader
        folders={folders}
        title={title}
        setTitle={setTitle}
        showDanger={showDanger}
        setShowDanger={setShowDanger}
        editMode={editMode}
      />
      <main className={styles.questionsEditor}>
        {inputs.map((input, index) => (
          <Question
            key={input._id}
            index={index}
            question={input.question}
            answer={input.answer}
            handleInputChange={handleInputChange}
          />
        ))}
        <div className={styles.solo}>
          <Button variant="secondary" type="button" onClick={handleClick}>
            + Add Card
          </Button>
        </div>
      </main>
      <QuestionFormFooter
        title={title}
        showDanger={showDanger}
        editMode={editMode}
        isAtMaximum={isAtMaximum}
        isAtMinimum={isAtMinimum}
      />
    </form>
  );
}

export default QuestionForm;
