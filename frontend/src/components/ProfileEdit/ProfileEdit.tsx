import { useState } from "react";
import { useAuthContext } from "../../hooks/useAuthContext";
import { updateUser } from "../../utils/user";
import Label from "../Label/Label";
import Input from "../Input/Input";
import Button from "../Button/Button";
import styles from "./ProfileEdit.module.css";

type ProfileEditProps = {
  name: string;
  fetchUser: any;
  toggleEditMode: any;
};

function ProfileEdit({ name, fetchUser, toggleEditMode }: ProfileEditProps) {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [newName, setNewName] = useState(() => name);
  const { user } = useAuthContext();

  const saveChanges = async () => {
    if (newName.length > 50) {
      setIsError(true);
    } else {
      setIsLoading(true);
      const userBody = {
        name: newName,
      };

      await updateUser(userBody, user.token);
      await fetchUser();
      toggleEditMode();
      setIsLoading(false);
    }
  };

  return (
    <div className={styles.edit}>
      <div>
        <Label htmlFor="name">Name</Label>
        <Input
          id="name"
          value={newName}
          onChange={(e) => setNewName(e.target.value)}
        />
      </div>
      {isError && (
        <p className={styles.error}>
          Profile name is too long. Maximum is 50 characters
        </p>
      )}
      <div className={styles.editControls}>
        <Button onClick={saveChanges} disabled={name === newName}>
          {isLoading ? "Saving..." : "Save"}
        </Button>
        <Button
          variant="secondary"
          onClick={toggleEditMode}
          disabled={isLoading}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
}

export default ProfileEdit;
