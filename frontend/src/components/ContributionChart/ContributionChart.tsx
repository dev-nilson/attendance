import { Link } from "react-router-dom";
import Chart from "../Chart/Chart";
import styles from "./ContributionChart.module.css";

type ContributionChartProps = {
  questions: Question[];
  slug: string | undefined;
};

export default function ContributionChart({
  questions,
  slug,
}: ContributionChartProps) {
  return (
    <div className={styles.contributionChart}>
      <p className={styles.label}>
        {questions.length} cards in{" "}
        <Link to={`/study/${slug}`} className={styles.title}>
          {slug}
        </Link>
      </p>
      <Chart data={questions} />
      <div className={styles.info}>
        <Link className={styles.link} to={"#"}>
          Learn how we count progress
        </Link>
        <div className={styles.example}>
          <span>Less</span>
          {Array.from({ length: 5 }, (_, index) => (
            <div key={index} className={styles.square}>
              <div
                className={styles.square}
                style={{
                  background: `rgba(57, 211, 83, ${index * 0.2})`,
                }}
              ></div>
            </div>
          ))}
          <span>More</span>
        </div>
      </div>
    </div>
  );
}
