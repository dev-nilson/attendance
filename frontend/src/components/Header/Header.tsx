import styles from "./Header.module.css";

type HeaderProps = {
  heading: string;
  subheading: string;
};

function Header({ heading, subheading }: HeaderProps) {
  return (
    <div className={styles.header}>
      <h1 className={styles.heading}>{heading}</h1>
      <p className={styles.subheading}>{subheading}</p>
    </div>
  );
}

export default Header;
