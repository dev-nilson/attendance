import styles from "./QuestionFormSkeleton.module.css";

function QuestionFormSkeleton() {
  return (
    <div className={styles.questionFormSkeleton}>
      <div className={styles.header} />
      {[...Array(5)].map((_, index) => (
        <div className={styles.questionsEditor} key={index}>
          <div className={styles.question} />
          <div className={styles.question} />
        </div>
      ))}
    </div>
  );
}

export default QuestionFormSkeleton;
