import { InputHTMLAttributes } from "react";
import styles from "./Input.module.css";

type InputProps = InputHTMLAttributes<HTMLInputElement> & {
  variant?: "primary" | "secondary";
};

function Input({ variant, children, ...props }: InputProps) {
  const inputClasses = `${styles.input} ${
    variant === "secondary" ? styles.secondaryInput : null
  }`;

  return <input className={inputClasses} {...props} />;
}

export default Input;
