import { useState } from "react";
import { Link } from "react-router-dom";
import Button from "../Button/Button";
import styles from "./ContributionCard.module.css";

type ContributionCardProps = {
  label: string;
  questions: Question[];
  sort: 1 | -1;
};

function ContributionCard({ label, questions, sort }: ContributionCardProps) {
  const [limit, setLimit] = useState(3);
  const questionsToDisplay = questions.slice(0, limit);

  return (
    <div className={styles.contributionCard}>
      <h3 className={styles.label}>{label}</h3>
      <div className={styles.questions}>
        {questionsToDisplay.map((question: Question) => (
          <div className={styles.question} key={question._id}>
            <div className={styles.text}>
              <Link
                key={question._id}
                className={styles.link}
                to={`/study/${question.slug}?question=${question.index}`}
              >
                {question.question}
              </Link>
              <p>
                {question.progress} {sort === 1 ? "right" : "wrong"} guesses
              </p>
            </div>
            <div
              className={styles.progress}
              style={{ width: question.progress }}
            />
          </div>
        ))}
        {questions.length > limit && (
          <Button variant="info" onClick={() => setLimit(limit + 3)}>
            Show more questions
          </Button>
        )}
      </div>
    </div>
  );
}

export default ContributionCard;
