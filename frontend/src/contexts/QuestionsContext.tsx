import { createContext, useReducer } from "react";

const initialState = { questions: [] as Array<Question> };
export const QuestionsContext = createContext<any>(null);

export const questionsReducer = (
  state: typeof initialState,
  action: ActionTypeQuestions
) => {
  switch (action.type) {
    case "SET_QUESTIONS":
      return {
        questions: action.payload,
      };
    case "CREATE_QUESTION":
      return {
        questions: [...state.questions, action.payload],
      };
    case "DELETE_QUESTION":
      return {
        questions: state.questions.filter(
          (question) => question._id !== action.payload._id
        ),
      };
    default:
      return state;
  }
};

export const QuestionsContextProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(questionsReducer, initialState);

  return (
    <QuestionsContext.Provider value={{ ...state, dispatch }}>
      {children}
    </QuestionsContext.Provider>
  );
};
