import { createContext, useReducer } from "react";

const initialState = { name: "" };
export const UserContext = createContext<any>(null);

export const userReducer = (
  state: typeof initialState,
  action: ActionTypeUser
) => {
  switch (action.type) {
    case "SET_NAME":
      return {
        name: action.payload,
      };
    default:
      return state;
  }
};

export const UserContextProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(userReducer, initialState);

  return (
    <UserContext.Provider value={{ ...state, dispatch }}>
      {children}
    </UserContext.Provider>
  );
};
