import { useState } from "react";
import { createContext, useReducer, useEffect } from "react";

const initialState = { user: null } as IAuth;
export const AuthContext = createContext<any>(null);

export const authReducer = (
  state: typeof initialState,
  action: ActionTypeAuth
) => {
  switch (action.type) {
    case "LOGIN":
      return { user: action.payload };
    case "LOGOUT":
      return { user: null };
    default:
      return state;
  }
};

export const AuthContextProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user")!);
    if (user) dispatch({ type: "LOGIN", payload: user });
    setIsLoading(false);
  }, []);

  return (
    <AuthContext.Provider value={{ ...state, isLoading, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};
