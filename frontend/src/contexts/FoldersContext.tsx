import { createContext, useReducer } from "react";

const initialState = { folders: [] as Array<Folder> };
export const FoldersContext = createContext<any>(null);

export const foldersReducer = (state: typeof initialState, action: any) => {
  switch (action.type) {
    case "SET_FOLDERS":
      return {
        folders: action.payload,
      };
    case "CREATE_FOLDER":
      return {
        folders: [...state.folders, action.payload],
      };
    case "DELETE_FOLDER":
      return {
        folders: state.folders.filter(
          (folder) => folder._id !== action.payload._id
        ),
      };
    default:
      return state;
  }
};

export const FoldersContextProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(foldersReducer, initialState);

  return (
    <FoldersContext.Provider value={{ ...state, dispatch }}>
      {children}
    </FoldersContext.Provider>
  );
};
