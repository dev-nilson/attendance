import { useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { signupUser } from "../utils/user";

export const useSignup = () => {
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const { dispatch } = useAuthContext();

  const signup = async (
    email: string,
    password: string,
    confirmation: string,
    name: string
  ) => {
    setLoading(true);
    setError("");

    const response = await signupUser({ email, password, confirmation, name });
    const data = await response.json();

    if (!response.ok) {
      setError(data.message);
    } else {
      localStorage.setItem("user", JSON.stringify(data));
      dispatch({ type: "LOGIN", payload: data });
    }

    setLoading(false);
  };

  return { signup, error, loading };
};
