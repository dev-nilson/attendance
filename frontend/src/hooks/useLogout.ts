import { useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { useQuestionsContext } from "./useQuestionsContext";

export const useLogout = () => {
  const { dispatch: authDispatch } = useAuthContext();
  const { dispatch: questionsDispatch } = useQuestionsContext();

  const logout = async () => {
    localStorage.removeItem("user");
    authDispatch({ type: "LOGOUT" });
    questionsDispatch({ type: "SET_QUESTIONS", payload: null });
  };

  return { logout };
};
