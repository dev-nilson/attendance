import { useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { loginUser } from "../utils/user";

export const useLogin = () => {
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const { dispatch } = useAuthContext();

  const login = async (email: string, password: string) => {
    setLoading(true);
    setError("");

    const response = await loginUser({ email, password });
    const data = await response.json();

    if (!response.ok) {
      setError(data.message);
    } else {
      localStorage.setItem("user", JSON.stringify(data));
      dispatch({ type: "LOGIN", payload: data });
    }

    setLoading(false);
  };

  return { login, error, loading };
};
