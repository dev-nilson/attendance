import { useEffect, useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { useUserContext } from "./useUserContext";

export const useUser = () => {
  const { user: userData, isLoading: isLoadingUser } = useAuthContext();
  const { dispatch } = useUserContext();
  const [user, setUser] = useState<any>(null);
  const [isLoading, setIsLoading] = useState(true);

  const fetchUser = async () => {
    const res = await fetch(
      `${import.meta.env.VITE_BASE_URL}/api/users`,
      {
        headers: {
          Authorization: `Bearer ${userData?.token}`,
        },
      }
    );

    const data = await res.json();

    if (res.ok) {
      setUser(data);
      dispatch({ type: "SET_QUESTIONS", payload: data });
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (isLoadingUser) return;
    fetchUser();
  }, [dispatch, isLoadingUser]);

  return { user, fetchUser, isLoading };
};
