import { useContext } from "react";
import { FoldersContext } from "../contexts/FoldersContext";

export const useFoldersContext = () => {
  const context = useContext(FoldersContext);

  if (!context) {
    throw Error("useFoldersContext must be used inside FoldersContextProvider");
  }

  return context;
};
