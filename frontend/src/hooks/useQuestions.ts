import { useEffect, useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { useQuestionsContext } from "./useQuestionsContext";
import { getQuestions } from "../utils/questions";

export const useQuestions = (slug: string) => {
  const { user, isLoading: isLoadingUser } = useAuthContext();
  const { dispatch } = useQuestionsContext();
  const [questions, setQuestions] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (isLoadingUser) return;

    const fetchQuestions = async () => {
      const response = await getQuestions(slug, user?.token);
      const data = await response.json();

      if (response.ok) {
        setQuestions(data.reverse());
        dispatch({ type: "SET_QUESTIONS", payload: data });
        setIsLoading(false);
      }
    };

    fetchQuestions();
  }, [dispatch, isLoadingUser]);

  return { questions, isLoading };
};
