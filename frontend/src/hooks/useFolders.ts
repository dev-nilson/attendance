import { useEffect, useState } from "react";
import { useAuthContext } from "./useAuthContext";
import { useFoldersContext } from "./useFoldersContext";

export const useFolders = () => {
  const { user, isLoading: isLoadingUser } = useAuthContext();
  const { dispatch } = useFoldersContext();
  const [folders, setFolders] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (isLoadingUser) return;

    const fetchFolder = async () => {
      const res = await fetch(
        `${import.meta.env.VITE_BASE_URL}/api/folders`,
        {
          headers: {
            Authorization: `Bearer ${user?.token}`,
          },
        }
      );

      const data = await res.json();

      if (res.ok) {
        setFolders(data);
        dispatch({ type: "SET_FOLDERS", payload: data });
        setIsLoading(false);
      }
    };

    fetchFolder();
  }, [dispatch, isLoadingUser]);

  return { folders, isLoading };
};
