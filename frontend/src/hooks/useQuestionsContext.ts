import { useContext } from "react";
import { QuestionsContext } from "../contexts/QuestionsContext";

export const useQuestionsContext = () => {
  const context = useContext(QuestionsContext);

  if (!context) {
    throw Error(
      "useQuestionsContext must be used inside QuestionsContextProvider"
    );
  }

  return context;
};
