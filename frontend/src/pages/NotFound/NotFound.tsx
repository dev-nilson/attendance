import { Link } from "react-router-dom";
import Button from "../../components/Button/Button";
import styles from "./NotFound.module.css";

function NotFound() {
  return (
    <div className={styles.notFound}>
      <h1 className={styles.code}>404</h1>
      <p className={styles.error}>
        This is not the website you are looking for.
      </p>
      <div className={styles.button}>
        <Link to="/">
          <Button>Go Home</Button>
        </Link>
      </div>
    </div>
  );
}

export default NotFound;
