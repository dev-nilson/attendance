import { useState } from "react";
import { Navigate } from "react-router-dom";
import { useAuthContext } from "../../hooks/useAuthContext";
import { useSignup } from "../../hooks/useSignup";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import Label from "../../components/Label/Label";
import Alert from "../../components/Alert/Alert";
import styles from "./Signup.module.css";

function Signup() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmation, setConfirmation] = useState("");
  const { signup, error, loading } = useSignup();
  const { user } = useAuthContext();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    await signup(email, password, confirmation, name);
  };

  if (user) return <Navigate to="/" replace />;

  return (
    <div className={styles.signup}>
      <div>
        <h1 className={styles.title}>Create account</h1>
      </div>
      {error && <Alert text={error} variant="danger" />}
      <form className={styles.form} onSubmit={handleSubmit}>
        <div className={styles.group}>
          <Label htmlFor="name">Name</Label>
          <Input
            type="text"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className={styles.group}>
          <Label htmlFor="email" required>Email</Label>
          <Input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className={styles.group}>
          <Label htmlFor="password" required>Password</Label>
          <Input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className={styles.group}>
          <Label htmlFor="confirmation" required>Confirm Password</Label>
          <Input
            type="password"
            id="confirmation"
            value={confirmation}
            onChange={(e) => setConfirmation(e.target.value)}
          />
        </div>
        <Button disabled={loading}>
          {loading ? "Signing up..." : "Sign up"}
        </Button>
      </form>
      <p className={styles.prompt}>
        Already have an account?{" "}
        <a className={styles.link} href="/login">
          Log in
        </a>
      </p>
    </div>
  );
}

export default Signup;
