import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useQuestions } from "../../hooks/useQuestions";
import QuestionForm from "../../components/QuestionForm/QuestionForm";
import Header from "../../components/Header/Header";
import NotFound from "../NotFound/NotFound";
import QuestionFormSkeleton from "../../components/QuestionFormSkeleton/QuestionFormSkeleton";
import styles from "./Edit.module.css";

function Edit() {
  const params = useParams();
  const { questions, isLoading } = useQuestions(params.slug || "");
  const [notFound, setNotFound] = useState(false);

  useEffect(() => {
    if (!isLoading && questions.length === 0) setNotFound(true);
  }, [isLoading]);

  return (
    <div>
      <div className={styles.editContainer}>
        {notFound ? (
          <NotFound />
        ) : (
          <>
            <Header
              heading="Edit an existing folder"
              subheading="A folder contains flashcards with questions and answers."
            />
            {isLoading ? (
              <QuestionFormSkeleton />
            ) : (
              <QuestionForm slug={params.slug} questions={questions} editMode />
            )}
          </>
        )}
      </div>
    </div>
  );
}

export default Edit;
