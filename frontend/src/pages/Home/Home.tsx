import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useFolders } from "../../hooks/useFolders";
import { useUser } from "../../hooks/useUser";
import { useFoldersContext } from "../../hooks/useFoldersContext";
import { useAuthContext } from "../../hooks/useAuthContext";
import { sort } from "../../utils/helpers";
import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import FolderCardSkeleton from "../../components/FolderCardSkeleton/FolderCardSkeleton";
import Select from "../../components/Select/Select";
import Profile from "../../components/Profile/Profile";
import ProfileSkeleton from "../../components/ProfileSkeleton/ProfileSkeleton";
import Folders from "../../components/Folders/Folders";
import styles from "./Home.module.css";

function Home() {
  const { isLoading } = useFolders();
  const { user, fetchUser, isLoading: isLoadingUser } = useUser();
  const { folders } = useFoldersContext();
  const { user: auth } = useAuthContext();
  const [data, setData] = useState<Folder[]>([]);
  const [sortBy, setSortBy] = useState("DEFAULT");
  const [country, setCountry] = useState("");
  const [currentTime, setCurrentTime] = useState<string>("00:00");

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentTime(
        new Date().toLocaleTimeString([], {
          hour: "2-digit",
          minute: "2-digit",
        })
      );
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    const getCountry = async () => {
      const response = await fetch("https://ipapi.co/json/");
      const data = await response.json();
      setCountry(data.country_name);
    };

    getCountry();
  }, []);

  useEffect(() => {
    setData(folders);
  }, [folders]);

  useEffect(() => {
    let sortedData: Folder[] = [];
    if (sortBy === "title") sortedData = sort(sortBy, "asc", [...data]);
    if (sortBy === "updatedAt") sortedData = sort(sortBy, "desc", [...data]);

    setData(sortedData);
  }, [sortBy]);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    const filteredFolders = folders.filter((folder: Folder) =>
      folder.title.includes(e.target.value)
    );
    setData(filteredFolders);
  };

  return (
    <div className={styles.home}>
      <div className={styles.homeLayout}>
        {isLoadingUser ? (
          <ProfileSkeleton />
        ) : (
          <Profile
            name={user?.name}
            email={auth?.email}
            foldersCount={folders.length}
            country={country}
            time={currentTime}
            fetchUser={fetchUser}
          />
        )}
        <div>
          <div className={styles.controls}>
            <Input placeholder="Find a folder..." onChange={handleSearch} />
            <Select value={sortBy} setValue={setSortBy} />
            <Link className={styles.link} to="/new">
              <Button>New</Button>
            </Link>
          </div>
          {isLoading ? (
            <FolderCardSkeleton />
          ) : data.length > 0 ? (
            <Folders data={data} />
          ) : (
            <div className={styles.message}>
              <p>No folders found.</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default Home;
