import React, { useEffect, useMemo, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { useParams, useSearchParams } from "react-router-dom";
import { useAuthContext } from "../../hooks/useAuthContext";
import { useQuestions } from "../../hooks/useQuestions";
import { updateQuestion } from "../../utils/questions";
import Button from "../../components/Button/Button";
import CardSkeleton from "../../components/CardSkeleton/CardSkeleton";
import Card from "../../components/Card/Card";
import NotFound from "../NotFound/NotFound";
import styles from "./Study.module.css";

function Study() {
  const params = useParams();
  const { user } = useAuthContext();
  const { questions, isLoading } = useQuestions(params.slug || "");
  const [searchParams] = useSearchParams();
  const start = parseInt(searchParams.get("question")!);
  const [notFound, setNotFound] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(2);
  const [count, setCount] = useState(2);

  const currentIndexRef = useRef(currentIndex);
  const canSwipe = useMemo(() => currentIndex >= 0, [currentIndex]);
  const childRefs: any = useMemo(
    () =>
      Array(count)
        .fill(0)
        .map((i) => React.createRef()),
    []
  );

  useEffect(() => {
    setCurrentIndex(questions.length - 1);
    setCount(questions.length);
  }, [questions.length]);

  useEffect(() => {
    if (!isLoading && questions.length === 0) setNotFound(true);
  }, [isLoading]);

  useEffect(() => {
    document.addEventListener("keyup", handleKeyUp);
    return () => {
      document.removeEventListener("keyup", handleKeyUp);
    };
  }, [currentIndex]);

  const handleKeyUp = (e: KeyboardEvent) => {
    if (e.key === "ArrowRight") swipe("right");
    if (e.key === "ArrowLeft") swipe("left");
  };

  const swipe = async (dir: "right" | "left") => {
    if (canSwipe && currentIndex < count)
      await childRefs[currentIndex].current.swipe(dir);
  };

  const updateCurrentIndex = (index: number) => {
    setCurrentIndex(index);
    currentIndexRef.current = index;
  };

  const swiped = (direction: string, question: Question, index: number) => {
    if (direction === "right") {
      const questionBody = {
        progress: question.progress + 1,
      };

      updateQuestion(question._id, questionBody, user.token);
    }

    updateCurrentIndex(index - 1);
  };

  const handleClick = () => {
    location.reload();
  };

  return (
    <div className={styles.study}>
      {isLoading ? (
        <CardSkeleton />
      ) : notFound ? (
        <NotFound />
      ) : (
        <div className={styles.studyContainer}>
          <div className={styles.cardsContainer}>
            {questions
              .slice(start - 1)
              .reverse()
              .map((question: Question, index: number) => (
                <Card
                  key={question._id}
                  question={question}
                  swiped={swiped}
                  index={index}
                  childRefs={childRefs}
                />
              ))}
            <div className={styles.controls}>
              <Button variant="secondary" onClick={handleClick}>
                Restart
              </Button>
              <Link to={`/dashboard/${params.slug}`}>
                <Button>See Progress</Button>
              </Link>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Study;
