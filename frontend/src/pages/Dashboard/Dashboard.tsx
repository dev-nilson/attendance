import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useQuestions } from "../../hooks/useQuestions";
import { sort } from "../../utils/helpers";
import NotFound from "../NotFound/NotFound";
import ContributionCard from "../../components/ContributionCard/ContributionCard";
import ContributionChart from "../../components/ContributionChart/ContributionChart";
import ContributionChartSkeleton from "../../components/ContributionChartSkeleton/ContributionChartSkeleton";
import ContributionCardSkeleton from "../../components/ContributionCardSkeleton/ContributionCardSkeleton";
import styles from "./Dashboard.module.css";

function Dashboard() {
  const params = useParams();
  const { questions, isLoading } = useQuestions(params.slug || "");
  const [notFound, setNotFound] = useState(false);
  const incorrectQuestions = JSON.parse(
    JSON.stringify(questions)
  ) as Question[];
  const correctQuestions = JSON.parse(JSON.stringify(questions)) as Question[];

  useEffect(() => {
    if (!isLoading && questions.length === 0) setNotFound(true);
  }, [isLoading]);

  return (
    <div className={styles.dashboard}>
      <div className={styles.dashboardContainer}>
        {notFound ? (
          <NotFound />
        ) : isLoading ? (
          <div className={styles.progress}>
            <ContributionChartSkeleton />
            <ContributionCardSkeleton label="Most guessed incorrectly" />
            <ContributionCardSkeleton label="Most guessed correctly" />
          </div>
        ) : (
          <div className={styles.progress}>
            <ContributionChart questions={questions} slug={params.slug} />
            <ContributionCard
              label="Most guessed incorrectly"
              questions={sort("progress", "asc", incorrectQuestions)}
              sort={-1}
            />
            <ContributionCard
              label="Most guessed correctly"
              questions={sort("progress", "desc", correctQuestions)}
              sort={1}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default Dashboard;
