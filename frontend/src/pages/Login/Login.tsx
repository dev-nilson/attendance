import { useState } from "react";
import { Navigate } from "react-router-dom";
import { useAuthContext } from "../../hooks/useAuthContext";
import { useLogin } from "../../hooks/useLogin";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import Label from "../../components/Label/Label";
import Alert from "../../components/Alert/Alert";
import styles from "./Login.module.css";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { login, error, loading } = useLogin();
  const { user } = useAuthContext();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    await login(email, password);
  };

  if (user) return <Navigate to="/" replace />;

  return (
    <div className={styles.login}>
      <div>
        <h1 className={styles.title}>Log in to Quizhub</h1>
      </div>
      {error && <Alert text={error} variant="danger" />}
      <form className={styles.form} onSubmit={handleSubmit}>
        <div className={styles.group}>
          <Label htmlFor="email" required>
            Email
          </Label>
          <Input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className={styles.group}>
          <Label htmlFor="password" required>
            Password
          </Label>
          <Input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <Button disabled={loading}>
          {loading ? "Logging in..." : "Log in"}
        </Button>
      </form>
      <p className={styles.prompt}>
        New to Quizhub?{" "}
        <a className={styles.link} href="/signup">
          Create an account
        </a>
      </p>
    </div>
  );
}

export default Login;
