import Header from "../../components/Header/Header";
import QuestionForm from "../../components/QuestionForm/QuestionForm";
import styles from "./New.module.css";

function New() {
  return (
    <div>
      <div className={styles.newContainer}>
        <Header
          heading="Create a new folder"
          subheading="A folder contains flashcards with questions and answers."
        />
        <QuestionForm editMode={false} />
      </div>
    </div>
  );
}

export default New;
