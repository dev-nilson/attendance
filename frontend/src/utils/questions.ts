export const getQuestions = async (slug: string, token: string) => {
  return await fetch(`${import.meta.env.VITE_BASE_URL}/api/questions/${slug}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const createQuestion = async (
  questionToCreate: Question,
  token: string
) => {
  await fetch(`${import.meta.env.VITE_BASE_URL}/api/questions`, {
    method: "POST",
    body: JSON.stringify(questionToCreate),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
};

export const deleteQuestions = async (folderId: string, token: string) => {
  await fetch(`${import.meta.env.VITE_BASE_URL}/api/questions/${folderId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const updateQuestion = async (
  questionId: string,
  questionBody: Partial<Question>,
  token: string
) => {
  await fetch(`${import.meta.env.VITE_BASE_URL}/api/questions/${questionId}`, {
    method: "PUT",
    body: JSON.stringify(questionBody),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
};
