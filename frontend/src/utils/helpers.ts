export const doesFieldExist = (array: any[], field: string, value: any) => {
  return array.some((item: any) => item[field] === value);
};

export const hasSpaces = (str: string) => {
  return str.includes(" ");
};

export const sort = <T, K extends keyof T>(
  field: K,
  order: "asc" | "desc",
  array: T[]
): T[] =>
  array.sort((a, b) => {
    const valueA = a[field];
    const valueB = b[field];

    if (typeof valueA !== "number" || typeof valueB !== "number") {
      throw new Error("valueA and valueB must be of type 'number'");
    }

    if (typeof order !== "string") {
      throw new Error("order must be of type 'string'");
    }

    return (valueA - valueB) * (order === "desc" ? -1 : 1);
  });
