export const updateUser = async (userBody: any, token: string) => {
  await fetch(`${import.meta.env.VITE_BASE_URL}/api/users`, {
    method: "PUT",
    body: JSON.stringify(userBody),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
};

export const signupUser = async (userBody: any) => {
  return await fetch(`${import.meta.env.VITE_BASE_URL}/api/users/signup`, {
    method: "POST",
    body: JSON.stringify(userBody),
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const loginUser = async (userBody: any) => {
  return await fetch(`${import.meta.env.VITE_BASE_URL}/api/users/login`, {
    method: "POST",
    body: JSON.stringify(userBody),
    headers: {
      "Content-Type": "application/json",
    },
  });
};
