export const createFolder = async (folderToCreate: Folder, token: string) => {
  const folder = await fetch(`${import.meta.env.VITE_BASE_URL}/api/folders`, {
    method: "POST",
    body: JSON.stringify(folderToCreate),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });

  return folder;
};

export const updateFolder = async (folderId: string, token: string) => {
  await fetch(`${import.meta.env.VITE_BASE_URL}/api/folders/${folderId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const deleteFolder = async (folderId: string, token: string) => {
  await fetch(`${import.meta.env.VITE_BASE_URL}/api/folders/${folderId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
