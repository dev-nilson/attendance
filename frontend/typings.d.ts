type Question = {
  _id: string;
  question: string;
  answer: string;
  progress: number;
  index: number;
  slug: string;
  folderId: string;
};

type Folder = {
  _id: string;
  title: string;
  slug: string;
  userId: string;
  updatedAt: Date;
};

interface IAuth {
  user: any;
}

type ActionTypeUser = { type: "SET_NAME"; payload: string };

type ActionTypeQuestions =
  | { type: "SET_QUESTIONS"; payload: Question[] }
  | { type: "CREATE_QUESTION"; payload: Question }
  | { type: "DELETE_QUESTION"; payload: Question };

type ActionTypeFolders =
  | { type: "SET_FOLDERS"; payload: Folder[] }
  | { type: "CREATE_FOLDER"; payload: Folder }
  | { type: "DELETE_FOLDER"; payload: Folder };

type ActionTypeAuth = { type: "LOGIN"; payload: any } | { type: "LOGOUT" };
